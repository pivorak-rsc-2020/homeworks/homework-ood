class AddUserIdToCurrentLocations < ActiveRecord::Migration[5.2]
  def change
    add_column :current_locations, :user_id, :integer
  end
end
