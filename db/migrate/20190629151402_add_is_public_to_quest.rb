class AddIsPublicToQuest < ActiveRecord::Migration[5.2]
  def change
    add_column :quests, :is_public, :boolean, default: false
  end
end
