class CurrentLocationSerializer
  include FastJsonapi::ObjectSerializer

  attributes :id

  attribute :latitude, &:lat

  attribute :longitude, &:lon
end
