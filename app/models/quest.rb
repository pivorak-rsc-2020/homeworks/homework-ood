class Quest < ApplicationRecord
  MAX_NAME_LENGTH = 50

  belongs_to :user

  validates :name, presence: true, length: { maximum: MAX_NAME_LENGTH }
  validates :description, presence: true
end
