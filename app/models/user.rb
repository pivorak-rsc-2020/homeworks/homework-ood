class User < ApplicationRecord
  has_many :quests, dependent: :destroy
  has_many :current_locations, dependent: :destroy

  has_secure_password
  validates :email, :password_digest, presence: true
end
