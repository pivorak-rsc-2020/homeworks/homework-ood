FactoryBot.define do
  factory :quest do
    name { Faker::TvShows::SiliconValley.invention }
    description { Faker::Movies::Lebowski.quote }
    user { FactoryBot.create(:user) }
  end
end
