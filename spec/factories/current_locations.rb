FactoryBot.define do
  factory :current_location do
    lon { Faker::Address.longitude }
    lat { Faker::Address.latitude }
    user { FactoryBot.create(:user) }
  end
end
