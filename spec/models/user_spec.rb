require 'rails_helper'

RSpec.describe User, type: :model, tdd: true do
  let!(:user) { FactoryBot.create(:user) }
  let!(:attributes) { %i[email password_digest role] }

  it 'has attributes' do
    attributes.each do |attribute|
      expect(user).to respond_to(attribute)
    end
  end

  it 'has a valid factory' do
    expect(user).to be_valid
  end

  context 'validations' do
    it { should validate_presence_of(:email) }
    it { should validate_presence_of(:password_digest) }
  end

  context 'relations' do
    it { is_expected.to have_many(:quests) }
    it { is_expected.to have_many(:current_locations) }
  end
end
